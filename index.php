<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S4 Activity</title>
</head>
<body>
	<h1>Building</h1>
	<p><?= $building->printName(); ?></p>
	<p><?= $building->setFloors("8"); ?></p>
	<p><?= $building->getFloors(); ?></p>

	<p><?= $building->setAddress("Timog Avenue, Quezon City, Philippines"); ?></p>
	<p><?= $building->getAddress(); ?></p>

	<p><?= $building->setName("Caswynn Complex"); ?></p>
	<p><?= $building->getName(); ?></p>

	<h1>Condominium</h1>
	<p><?= $condominium->printName(); ?></p>
	<p><?= $condominium->setFloors("5"); ?></p>
	<p><?= $condominium->getFloors(); ?></p>

	<p><?= $condominium->setAddress("Buendia Avenue, Makati City, Philippines"); ?></p>
	<p><?= $condominium->getAddress(); ?></p>

	<p><?= $condominium->setName("Enzo Tower"); ?></p>
	<p><?= $condominium->getName(); ?></p>


</body>
</html>